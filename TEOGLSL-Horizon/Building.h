#pragma once
#include "Transform.h"
#include <GL/glew.h>		// include GLEW and new version of GL on Windows
#include <GLFW/glfw3.h> // GLFW helper library
#include "BoundingBox.h"

class Building
{
private:
	Vector3 position;
	Vector3 size;

	Transform transform; 
	BoundingBox bb;

	GLfloat* points;
	int pointsCount;

	GLuint points_vbo;
	GLuint normals_vbo;
	GLuint visibleFlags_vbo;

	GLuint vao;
public:
	Building(Vector3 position, Vector3 size)
	{
		this->position = position;
		this->size = size;

		transform.Move(position);

		//bb.Set(position + Vector3(0, size.y / 2.0f, 0), size);
		bb.Set(size, transform);

		float hsx = size.x / 2.0f;
		//float hsy = size.y / 2.0f;
		float hsz = size.x / 2.0f;

		pointsCount = 36;

		GLfloat points[] =
		{
			-hsx, size.y, -hsz, -hsx, 0, -hsz, hsx,	0, -hsz,
			hsx, 0, -hsz, hsx, size.y, -hsz, -hsx, size.y, -hsz,

			-hsx, 0, hsz, -hsx, 0, -hsz, -hsx, size.y, -hsz,
			-hsx, size.y, -hsz, -hsx, size.y, hsz, -hsx, 0, hsz,

			hsx, 0, -hsz, hsx,	0, hsz,	hsx, size.y, hsz,
			hsx, size.y, hsz, hsx, size.y, -hsz, hsx, 0, -hsz,

			-hsx, 0, hsz, -hsx, size.y,	hsz, hsx, size.y, hsz,
			hsx, size.y, hsz, hsx, 0, hsz, -hsx, 0, hsz,

			-hsx, size.y, -hsz, hsx, size.y, -hsz, hsx,	size.y, hsz,
			hsx, size.y, hsz, -hsx, size.y,	hsz, -hsx, size.y, -hsz,

			-hsx, 0, -hsz, -hsx, 0, hsz, hsx, 0, -hsz,
			hsx, 0, -hsz, -hsx, 0, hsz,	hsx, 0, hsz
		};

		this->points = new GLfloat[pointsCount * 3];
		memcpy(this->points, points, (pointsCount * 3) * sizeof(GLfloat));

		float normals[] =
		{
			0, 0, -1, 0, 0, -1, 0, 0, -1,
			0, 0, -1, 0, 0, -1, 0, 0, -1,

			-1, 0, 0, -1, 0, 0, -1, 0, 0,
			-1, 0, 0, -1, 0, 0, -1, 0, 0,

			1, 0, 0, 1, 0, 0, 1, 0, 0,
			1, 0, 0, 1, 0, 0, 1, 0, 0,

			0, 0, 1, 0, 0, 1, 0, 0, 1,
			0, 0, 1, 0, 0, 1, 0, 0, 1,

			0, 1, 0, 0, 1, 0, 0, 1, 0,
			0, 1, 0, 0, 1, 0, 0, 1, 0,

			0, -1, 0, 0, -1, 0, 0, -1, 0,
			0, -1, 0, 0, -1, 0, 0, -1, 0
		};

		GLint visibleFlag[] =
		{
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
					//		  
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
					//		  
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
					//		  
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
					//		  
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
					//		  
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
			1, 1, 1,// 1, 1, 1, 1, 1, 1,
		};

		
		glGenBuffers(1, &points_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
		glBufferData(GL_ARRAY_BUFFER, 3 * pointsCount * sizeof(GLfloat), &points, GL_STATIC_DRAW);

		glGenBuffers(1, &normals_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, normals_vbo);
		glBufferData(GL_ARRAY_BUFFER, 3 * pointsCount * sizeof(GLfloat), normals, GL_STATIC_DRAW);

		glGenBuffers(1, &visibleFlags_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, visibleFlags_vbo);
		glBufferData(GL_ARRAY_BUFFER, pointsCount * sizeof(GLint), visibleFlag, GL_STATIC_DRAW);
		
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, normals_vbo);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ARRAY_BUFFER, visibleFlags_vbo);
		glVertexAttribIPointer(2, 1, GL_INT, 0, NULL);
		glEnableVertexAttribArray(2);
	}

	~Building()
	{

	}

	void Render(GLint model_mat_location)
	{
		glUniformMatrix4fv(model_mat_location, 1, GL_FALSE, transform.GetModelMatrix().m);
		//glDepthMask(GL_FALSE);
		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLES, 0, pointsCount);
	}

	void UpdateVerticesVisibility(bool (*condition)(Vector3 vertex))
	{
		GLint* visibleFlag = new GLint[pointsCount];

		for (int i = 0; i < pointsCount * 3; i += 3)
		{
			vec4 translatedPoint = transform.GetModelMatrix() * vec4(points[i], points[i + 1], points[i + 2], 1.0f);
			Vector3 p(translatedPoint.v[0], translatedPoint.v[1], translatedPoint.v[2]);

			visibleFlag[i / 3] = (condition(p)) ? 0 : 1;
		}

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, visibleFlags_vbo);
		glBufferData(GL_ARRAY_BUFFER, pointsCount * sizeof(GLint), visibleFlag, GL_STATIC_DRAW);
		//glBufferSubData(GL_ARRAY_BUFFER, 0, pointsCount * sizeof(GLint), visibleFlag);
	}

	Transform* GetTransform() 
	{
		return &transform;
	}

	BoundingBox GetBoundingBox()
	{
		return bb;
	}
};

