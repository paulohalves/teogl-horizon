#pragma once
#include "Vector3.h"
#include <glm.hpp>
#include <GL/glew.h> 
#include "maths_funcs.h"

class Transform
{
private:
	Vector3 position;
	Vector3 scale;
	Vector3 rotation;

public:
	Transform() {
		scale = Vector3(1, 1, 1);
	}
	Transform(Transform& transform) 
	{
		Set(transform);
	}
	~Transform();

	Vector3* GetPosition();
	Vector3* GetScale();
	Vector3* GetRotation();

	//local to world
	mat4 GetModelMatrix() {
		mat4 R = rotate_x_deg(identity_mat4(), rotation.x) * rotate_y_deg(identity_mat4(), rotation.y) * rotate_z_deg(identity_mat4(), rotation.z);
		mat4 P = translate(identity_mat4(), vec3(position.x, position.y, position.z));
		mat4 S = scalev(identity_mat4(), vec3(scale.x, scale.y, scale.z));
		return P*R*S;
	}

	//world to local
	mat4 GetModelInverseMatrix() {
		return inverse(GetModelMatrix());
	}

	//Transforms position from local space to world space.
	Vector3 TransformPosition(Vector3 position)
	{
		vec4 v = vec4(position.x, position.y, position.z, 1);
		v = GetModelMatrix() * v;
		return Vector3(v.v[0], v.v[1], v.v[2]);
	}
	//Transforms position from world space to local space.
	Vector3 InverseTransformPosition(Vector3 position)
	{
		vec4 v = vec4(position.x, position.y, position.z, 1);
		v = GetModelInverseMatrix() * v;
		return Vector3(v.v[0], v.v[1], v.v[2]);
	}

	void Rotate(Vector3 angle) {

		Vector3 d = position;
		position = Vector3();

		rotation.x += angle.x;
		if (rotation.x >= 360) rotation.x -= 360;
		if (rotation.x <= -360) rotation.x += 360;

		rotation.y += angle.y;
		if (rotation.y >= 360) rotation.y -= 360;
		if (rotation.y <= -360) rotation.y += 360;

		rotation.z += angle.z;
		if (rotation.z >= 360) rotation.z -= 360;
		if (rotation.z <= -360) rotation.z += 360;

		position = d;
		
	}

	void Move(Vector3 o) {
		position += o;
	}

	void RevertPosX() {
		position = Vector3(-position.x, position.y, position.z);
	}
	void RevertPosZ() {
		position = Vector3(position.x, position.y, -position.z);
	}

	Vector3 GetForward() {

		float x = sinf(rotation.y * 0.0174533f);
		float z = cosf(rotation.y * 0.0174533f);

		return Vector3(x, 0, z);
	}

	Vector3 GetRight() {

		Vector3 forward = GetForward();

		vec3 r = cross(vec3(forward.x, forward.y, forward.z), vec3(0, 1, 0));

		return Vector3(r.v[0], r.v[1], r.v[2]);
	}

	void SetScale(Vector3 s) {
		scale = s;
	}

	void Set(Transform& transform)
	{
		position = transform.position;
		rotation = transform.rotation;
		scale = transform.scale;
	}
};

