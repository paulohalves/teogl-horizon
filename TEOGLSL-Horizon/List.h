#pragma once
#include <stdlib.h>
#include <cstring>

template<class T> class List
{
private:
	T* values;
	int size;
public:

	List<T>()
	{
		size = 0;
	}
	List<T>(T* arr, int lenght)
	{
		size = lenght;
		values = (T*)realloc(values, size * sizeof(T));
		memcpy(&values[0], arr, lenght * sizeof(T));
	}

	T operator [](int i)
	{
		return values[i];
	}
	T At(int i)
	{
		return values[i];
	}
	int Find(T value, bool (*equals)(T& a, T& b))
	{
		for (int i = 0; i < size; i++)
		{
			T v = values[i];
			if (equals(v, value))
			{
				return i;
			}
		}
		return -1;
	}
	void Add(T value)
	{
		size++;
		values = (T*)realloc(values, size * sizeof(T));
		values[size - 1] = value;
	}
	void AddAt(T value, int index)
	{
		if (index > size)
		{
			index = size;
		}
		else if (index < 0)
		{
			index = 0;
		}

		int lastS = (size - index);
		T* last = (T*)malloc(lastS * sizeof(T));

		if (lastS > 0) 
		{
			memcpy(last, &values[index], lastS * sizeof(T));
		}

		size++;
		values = (T*)realloc(values, size * sizeof(T));

		values[index] = value;

		memcpy(&values[(index + 1)], last, lastS * sizeof(T));
	}
	void AddRange(T* arr, int lenght)
	{
		values = (T*)realloc(values, (size + lenght) * sizeof(T));
		memcpy(&values[size], arr, lenght * sizeof(T));
		size += lenght;
	}
	void RemoveAt(int index)
	{
		if (index >= size)
		{
			index = size - 1;
		}
		else if (index < 0)
		{
			index = 0;
		}

		int lastS = (size - (index + 1));
		T* last = (T*)malloc(lastS * sizeof(T));

		if (lastS > 0)
		{
			memcpy(last, &values[(index + 1)], lastS * sizeof(T));
		}

		size--;
		values = (T*)realloc(values, size * sizeof(T));

		memcpy(&values[index], last, lastS * sizeof(T));
	}
	
	void RemoveTop()
	{
		size--;
		values = (T*)realloc(values, size * sizeof(T));
	}

	void ShellSort(bool (*condition)(T& a, T& b))
	{
		int i, j;
		T key;		

		int h = 1;
		while (h < size) {
			h = 3 * h + 1;
		}
		while (h > 0) {
			for (i = h; i < size; i++) {
				key = values[i];
				j = i;
				while (j > h - 1 && condition(key, values[j - h])) {
					values[j] = values[j - h];
					j = j - h;
				}
				values[j] = key;
			}
			h = h / 3;
		}
	}

	void InsertionSort(bool (*condition)(T& a, T& b))
	{
		int i, j;
		T key;
		
		for (i = 1; i < size; i++) {
			key = values[i];
			j = i - 1;
			while (j >= 0 && condition(key, values[j])) {
				values[j + 1] = values[j];
				j = j - 1;
			}
			values[j + 1] = key;
		}
	}

	void Clear()
	{		
		size = 0;
		values = (T*)realloc(values, size * sizeof(T));
	}

	int Size()
	{
		return size;
	}

	T* ToArray()
	{
		T* arr = new T[size];
		memcpy(arr, values, size * sizeof(T));
		return arr;
	}
};

