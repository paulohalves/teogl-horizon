#pragma once
#include "Transform.h"

struct BoundingBox
{
	Vector3 lbf;
	Vector3 lbn;
	Vector3 ltf;
	Vector3 ltn;
	Vector3 rbf;
	Vector3 rbn;
	Vector3 rtf;
	Vector3 rtn;
	Vector3 center;

	Transform* transform;

	void Set(Vector3 size, Transform& transform) 
	{
		this->transform = &transform;

		float hsx = size.x / 2.0f;
		float hsz = size.x / 2.0f;

		lbf = Vector3(-hsx, 0, hsz);
		lbn = Vector3(-hsx, 0, -hsz);
		ltf = Vector3(-hsx, size.y, hsz);
		ltn = Vector3(-hsx, size.y, -hsz);
		rbf = Vector3(hsx, 0, hsz);
		rbn = Vector3(hsx, 0, -hsz);
		rtf = Vector3(hsx, size.y, hsz);
		rtn = Vector3(hsx, size.y, -hsz);
		center = Vector3(0, size.y / 2, 0);
	}

	//bool TestPoint(Vector3 p) {
	//	bool b = false;
	//	if (p.x >= ax && p.x <= bx) {
	//		if (p.y >= ay && p.y <= by) {
	//			if (p.z >= az && p.z <= bz) {
	//				b = true;
	//			}
	//		}
	//	}
	//	return b;
	//}
	//
	//bool TestBound(BoundingBox p) {
	//	bool b = false;
	//	if (p.ax >= ax && p.ax <= bx) {
	//		if (p.ay >= ay && p.ay <= by) {
	//			if (p.az >= az && p.az <= bz) {
	//				b = true;
	//			}
	//		}
	//	}
	//
	//	if (p.bx >= ax && p.bx <= bx) {
	//		if (p.by >= ay && p.by <= by) {
	//			if (p.bz >= az && p.bz <= bz) {
	//				b = true;
	//			}
	//		}
	//	}
	//
	//	return b;
	//}

	Vector3* GetUpperPoints()
	{
		//return new Vector3[2]{ Vector3(ax, by, (az + bz) / 2.0f), Vector3(bx, by, (az + bz) / 2.0f) };
		return new Vector3[4]{ transform->TransformPosition(ltf), transform->TransformPosition(ltn), transform->TransformPosition(rtn), transform->TransformPosition(rtf)};
	}

	Vector3 GetCenter()
	{
		return transform->TransformPosition(center);
	}
};
