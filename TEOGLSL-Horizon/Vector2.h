#pragma once
class Vector2
{
public:
	float x, y;

	Vector2() {
		x = 0;
		y = 0;
	}
	Vector2(float _x, float _y) {
		x = _x;
		y = _y;
	}

	bool AddValue(float v) {
		if (x == 0) {
			x = v;

			return true;
		}
		else if (y == 0) {
			y = v;

			return true;
		}

		return false;
	}
	~Vector2();
};

