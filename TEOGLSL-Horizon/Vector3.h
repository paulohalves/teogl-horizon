#pragma once
#include <cmath>
#include "List.h"
#include "maths_funcs.h"

class Vector3
{
public:
	float x, y, z;

	Vector3() {
		x = 0;
		y = 0;
		z = 0;
	}
	Vector3(float _x, float _y, float _z) {
		x = _x;
		y = _y;
		z = _z;
	}

	Vector3 Forward()
	{
		return Vector3(0, 0, 1);
	}

	bool AddValue(float v) {
		if (x == 0) {
			x = v;

			return true;
		}
		else if (y == 0) {
			y = v;

			return true;
		}
		else if (z == 0) {
			z = v;

			return true;
		}

		return false;
	}

	Vector3& operator+= (const Vector3& v) {
		this->x += v.x;
		this->y += v.y;
		this->z += v.z;
		return *this;
	}

	Vector3 operator* (const float f)
	{
		Vector3 r;

		r.x = this->x * f;
		r.y = this->y * f;
		r.z = this->z * f;
		return r;
	}

	Vector3 operator+ (const Vector3& v)
	{
		Vector3 r;

		r.x = this->x + v.x;
		r.y = this->y + v.y;
		r.z = this->z + v.z;

		return r;
	}

	bool operator== (const Vector3& v)
	{
		return (this->x == v.x && this->y == v.y && this->z == v.z);
	}
	bool operator!= (const Vector3& v)
	{
		return *this != v;
	}

	//Vector3 operator= (const Vector3& v)
	//{
	//	this->x = v.x;
	//	this->y = v.y;
	//	this->z = v.z;
	//
	//	return *this;
	//}

	float Magnitude() {
		return sqrtf(powf(x, 2) + powf(y, 2) + powf(z, 2));
	}

	static float DistanceSqrt(Vector3 a, Vector3 b)
	{
		return powf(b.x-a.x, 2) + powf(b.y-a.y, 2) + powf(b.z-a.z, 2);
	}

	static float* ToArray(List<Vector3> list)
	{
		float* arr = new float[list.Size() * 3];
		for (int i = 0; i < list.Size(); i++)
		{
			arr[i * 3] = list[i].x;
			arr[(i * 3) + 1] = list[i].y;
			arr[(i * 3) + 2] = list[i].z;
		}
		return arr;
	}

	static vec3* ToVecArray(List<Vector3> list)
	{
		vec3* arr = new vec3[list.Size()];
		for (int i = 0; i < list.Size(); i++)
		{
			arr[i] = vec3(list[i].x, list[i].y, list[i].z);
		}
		return arr;
	}

	~Vector3();
};

