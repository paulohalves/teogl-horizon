
#include <iostream>
#include "gl_utils.h"
#include "maths_funcs.h"
#include <GL/glew.h>	
#include <GLFW/glfw3.h> 
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define _USE_MATH_DEFINES
#include <math.h>
#define GL_LOG_FILE "gl.log"

#include "List.h"

#include "Building.h"
#include "Camera.h"
#include "Horizon.h"

int g_gl_width = 640;
int g_gl_height = 480;
GLFWwindow *g_window = NULL;

Camera* camera = NULL;
Transform* eye;
Horizon* horizon = NULL;

int main() {
	restart_gl_log();
	/*------------------------------start GL
	 * context------------------------------*/
	start_gl();

	bool discardPixels = false;
	std::string answ = "";

	while (answ == "")
	{
		std::cout << "Discard Pixels? (y/n)" << std::endl;
		std::cin >> answ;

		if (answ == "y")
		{
			discardPixels = true;
		}
		else if (answ == "n")
		{
			discardPixels == false;
		}
		else
		{
			answ = "";
		}
	}

	List<Building*> buildings;

	buildings.Add(new Building(Vector3(0, 0, 0), Vector3(1, 2, 1)));
	buildings.Add(new Building(Vector3(0, 0, 2), Vector3(1, 3, 1)));
	buildings.Add(new Building(Vector3(2, 0, 1), Vector3(1, 3, 1)));
	buildings.Add(new Building(Vector3(-2, 0, -1), Vector3(1, 3, 1)));
	buildings.Add(new Building(Vector3(-1.5, 0, 1.5), Vector3(1, 1, 1)));
	buildings.Add(new Building(Vector3(1.5, 0, -1.5), Vector3(1, 1, 1)));

	char vertex_shader[1024 * 256];
	char fragment_shader[1024 * 256];
	parse_file_into_str( "test_vs.glsl", vertex_shader, 1024 * 256 );
	parse_file_into_str( "test_fs.glsl", fragment_shader, 1024 * 256 );

	GLuint vs = glCreateShader( GL_VERTEX_SHADER );
	const GLchar *p = (const GLchar *)vertex_shader;
	glShaderSource( vs, 1, &p, NULL );
	glCompileShader( vs );

	int params = -1;
	glGetShaderiv( vs, GL_COMPILE_STATUS, &params );
	if ( GL_TRUE != params ) {
		fprintf( stderr, "ERROR: GL shader index %i did not compile\n", vs );
		print_shader_info_log( vs );
		return 1; 
	}

	GLuint fs = glCreateShader( GL_FRAGMENT_SHADER );
	p = (const GLchar *)fragment_shader;
	glShaderSource( fs, 1, &p, NULL );
	glCompileShader( fs );

	// check for compile errors
	glGetShaderiv( fs, GL_COMPILE_STATUS, &params );
	if ( GL_TRUE != params ) {
		fprintf( stderr, "ERROR: GL shader index %i did not compile\n", fs );
		print_shader_info_log( fs );
		return 1; // or exit or something
	}

	GLuint shader_programme = glCreateProgram();
	glAttachShader( shader_programme, fs );
	glAttachShader( shader_programme, vs );
	glLinkProgram( shader_programme );

	glGetProgramiv( shader_programme, GL_LINK_STATUS, &params );
	if ( GL_TRUE != params ) {
		fprintf( stderr, "ERROR: could not link shader programme GL index %i\n",
						 shader_programme );
		print_programme_info_log( shader_programme );
		return false;
	}

	GLint view_mat_location = glGetUniformLocation( shader_programme, "view_mat" );
	GLint proj_mat_location = glGetUniformLocation( shader_programme, "projection_mat" );
	GLint model_mat_location = glGetUniformLocation(shader_programme, "model_mat");
	GLint horizon_location = glGetUniformLocation(shader_programme, "horizon");
	GLint horizonSize_location = glGetUniformLocation(shader_programme, "horizonSize");
	GLint windowSize_location = glGetUniformLocation(shader_programme, "window_size");
	GLint cameraEye_location = glGetUniformLocation(shader_programme, "cameraEye_view_mat");

	GLint color_location = glGetUniformLocation(shader_programme, "visible_color");
	GLint discard_location = glGetUniformLocation(shader_programme, "discardFragment");

	glUseProgram( shader_programme );

	float aspectRatio = (float)g_gl_width / (float)g_gl_height;
	camera = new Camera(aspectRatio, view_mat_location, proj_mat_location);

	camera->Move(Vector3(0, -1, -8));

	horizon = new Horizon(shader_programme);

	glEnable( GL_CULL_FACE );
	glCullFace( GL_BACK );	
	glFrontFace(GL_CW);		

	glEnable(GL_DEPTH_TEST); 
	//glDepthFunc(GL_LESS);	

	eye = new Transform(*camera->GetTransform());

	eye->Set(*camera->GetTransform());
	horizon->Update(*eye, camera->GetProjectionMatrix(), aspectRatio);
	glUniformMatrix4fv(cameraEye_location, 1, GL_FALSE, eye->GetModelInverseMatrix().m);

	//horizon->Update(*eye, identity_mat4(), 1);
	glUniform1i(discard_location, discardPixels ? 1 : 0);

	int drawLoop = buildings.Size();
	bool inputAdd = false;
	bool inputRemove = false;

	bool cam_moved = true;
	while ( !glfwWindowShouldClose( g_window ) ) {
		static double previous_seconds = glfwGetTime();
		double current_seconds = glfwGetTime();
		double elapsed_seconds = current_seconds - previous_seconds;
		previous_seconds = current_seconds;

		_update_fps_counter("Occlusion Horizon by Paulo H.Alves", g_window );

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glViewport( 0, 0, g_gl_width, g_gl_height );
		glUniform2f(windowSize_location, g_gl_width, g_gl_height);

		glUseProgram( shader_programme );

		//sort objects by distance
		buildings.ShellSort(
			[](Building*& a, Building*& b) ->
			bool {
				Vector3 aPos = *(a->GetTransform()->GetPosition());
				Vector3 bPos = *(b->GetTransform()->GetPosition());

				Vector3 camPos = *(eye->GetPosition());

				return Vector3::DistanceSqrt(aPos, camPos) < Vector3::DistanceSqrt(bPos, camPos);
			});

		horizon->ClearHorizon();

		//glDepthMask(false);
		glDepthFunc(GL_LESS);
		for (int i = 0; i < buildings.Size() && i < drawLoop; i++)
		{
			glUniform1fv(horizon_location, horizon->GetHorizon()->Size() * 3, Vector3::ToArray(*horizon->GetHorizon()));
			glUniform1i(horizonSize_location, horizon->GetHorizon()->Size() * 3);

			if (i == drawLoop - 1)
			{
				glUniform3f(color_location, 0, 1, 0);
			}
			else 
			{
				glUniform3f(color_location, .5, .5, .5);
			}
			//About here we can decide whether draw or discard the object

			buildings[i]->Render(model_mat_location);
			horizon->AddToHorizon(buildings[i]->GetBoundingBox());
		}

		//glDepthMask(true);
		glDepthFunc(GL_ALWAYS);
		horizon->Render(model_mat_location);

		glfwPollEvents();

		cam_moved = false;
		if (glfwGetKey(g_window, GLFW_KEY_SPACE)) {
			eye->Set(*camera->GetTransform());
			horizon->Update(*eye, camera->GetProjectionMatrix(), aspectRatio);
			drawLoop = buildings.Size();
			glUniformMatrix4fv(cameraEye_location, 1, GL_FALSE, eye->GetModelInverseMatrix().m);

			//horizon->Update(*eye, identity_mat4(), 1);
		}

		if (glfwGetKey(g_window, GLFW_KEY_UP) && !inputAdd)
		{
			drawLoop++;
			if (drawLoop > buildings.Size())
			{
				drawLoop = buildings.Size();
			}
			inputAdd = true;
		}
		else if (glfwGetKey(g_window, GLFW_KEY_UP) == GLFW_RELEASE)
		{
			inputAdd = false;
		}
		
		if (glfwGetKey(g_window, GLFW_KEY_DOWN) && !inputRemove)
		{
			drawLoop--;
			if (drawLoop < 0)
			{
				drawLoop = 0;
			}
			inputRemove = true;
		}
		else if (glfwGetKey(g_window, GLFW_KEY_DOWN) == GLFW_RELEASE)
		{
			inputRemove = false;
		}

		if ( glfwGetKey( g_window, GLFW_KEY_W ) ) {
			camera->Move(camera->GetTransform()->GetForward() * elapsed_seconds);
		}
		if ( glfwGetKey( g_window, GLFW_KEY_S ) ) {
			camera->Move(camera->GetTransform()->GetForward() * -1.0f * elapsed_seconds);
		}
		if (glfwGetKey(g_window, GLFW_KEY_A)) {
			camera->Move(camera->GetTransform()->GetRight() * -1.0f * elapsed_seconds);
		}
		if (glfwGetKey(g_window, GLFW_KEY_D)) {
			camera->Move(camera->GetTransform()->GetRight() * elapsed_seconds);
		}
		if ( glfwGetKey( g_window, GLFW_KEY_LEFT ) ) {
			camera->RotateY(90.0f * elapsed_seconds);
		}
		if ( glfwGetKey( g_window, GLFW_KEY_RIGHT ) ) {
			camera->RotateY(-90.0f * elapsed_seconds);
		}

		if ( GLFW_PRESS == glfwGetKey( g_window, GLFW_KEY_ESCAPE ) ) {
			glfwSetWindowShouldClose( g_window, 1 );
		}

		glfwSwapBuffers( g_window );
	}

	glfwTerminate();
	return 0;
}
