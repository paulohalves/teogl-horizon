#pragma once
#include <GL/glew.h>		// include GLEW and new version of GL on Windows
#include <GLFW/glfw3.h> // GLFW helper library
#include "BoundingBox.h"
#include "Transform.h"
#include <stdlib.h>

class Horizon
{
private:
	Transform transform;
	List<Vector3> horizon;
	List<Vector3> addedHorizon;

	GLuint points_vbo;
	GLuint vao;

	GLuint ad_points_vbo;
	GLuint ad_vao;


	GLuint horizonFlag_location;
	GLuint horizonColor_location;

	float aspectRatio;
	mat4 projectionMatrix;
	mat4 viewMatrix;

	const float ApproximatelyDelta = 0.0001f;

							//L1.p0			L1.p1		L2.p0		L2.p1
	bool LinesIntersection(Vector3 lA, Vector3 lB, Vector3 rA, Vector3 rB, Vector3& point) {

		float det = (rB.x - rA.x) * (lB.y - lA.y)
			- (rB.y - rA.y) * (lB.x - lA.x);

		if (det == 0) {
			return false; // n�o h� intersec��o
		}

		float s = ((rB.x - rA.x) * (rA.y - lA.y)
			- (rB.y - rA.y) * (rA.x - lA.x)) / det;

		float r = ((lB.x - lA.x) * (lA.y - rA.y)
			- (lB.y - lA.y) * (lA.x - rA.x)) / (-det);

		if ((s > 1) || (s < 0) || (r > 1) || (r < 0))
		{
			return false; // n�o h� interse��o
		}

		point.x = lA.x + (lB.x - lA.x) * s;
		point.y = lA.y + (lB.y - lA.y) * s;

		return true;
	}

	bool Above(Vector3 a, Vector3 b, Vector3 c, bool allowColinear = false)
	{
		if (c.x < a.x || c.x > b.x)
		{
			return false;
		}

		// ccw > 0: counter-clockwise; ccw < 0: clockwise; ccw = 0: collinear
		float ccw = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);

		if (std::abs(ccw) < ApproximatelyDelta)
		{
			if (allowColinear /*|| (c.y > a.y && c.y > b.y)*/)
			{
				return true;
			}
		}

		return ccw > 0;
	}

	bool BelowSegment(Vector3 a, Vector3 b, Vector3 c, bool collinearIsAbove = false)
	{

		if ((c.x == a.x || c.x == b.x) && collinearIsAbove)
		{
			return false;
		}


		if (c.x < a.x || c.x > b.x)
		{
			return false;
		}

		// ccw > 0: counter-clockwise; ccw < 0: clockwise; ccw = 0: collinear
		float ccw = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);

		if (std::abs(ccw) < ApproximatelyDelta)
		{
			if (collinearIsAbove /*|| (c.y > a.y && c.y > b.y)*/)
			{
				return false;
			}
		}

		return ccw < 0;
	}

	void GetAdjacentPoints(Vector3 vertex, int& leftIdx, int& rightIdx, int startBy = 0)
	{
		leftIdx = startBy;
		rightIdx = horizon.Size() - 1;
		for (int i = startBy; i < horizon.Size(); i++)
		{
			if (horizon.At(i).x < vertex.x)
			{
				//left = horizon.At(i);
				leftIdx = i;
			}
			else if (horizon.At(i).x > vertex.x)
			{
				//right = horizon.At(i);
				rightIdx = i;

				break;
			}
			else
			{
				if (horizon.At(leftIdx).x == vertex.x)
				{
					//right = horizon.At(i);
					rightIdx = i;
					break;
				}
				else
				{
					//left = horizon.At(i);
					leftIdx = i;
				}
			}
		}
	}

	void UpdatePoints()
	{
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
		glBufferData(GL_ARRAY_BUFFER, horizon.Size() * 3 * sizeof(GLfloat), Vector3::ToArray(horizon), GL_STATIC_DRAW);

		glBindVertexArray(ad_vao);
		glBindBuffer(GL_ARRAY_BUFFER, ad_points_vbo);
		glBufferData(GL_ARRAY_BUFFER, addedHorizon.Size() * 3 * sizeof(GLfloat), Vector3::ToArray(addedHorizon), GL_STATIC_DRAW);
	}

	Vector3 TransformPoint(Vector3 point)
	{		
		vec4 clipSpacePos = projectionMatrix * (viewMatrix * vec4(point.x, point.y, point.z, 1));
		vec3 ndcSpacePos = vec3(clipSpacePos) / clipSpacePos.v[3];
		return Vector3(ndcSpacePos.v[0], ndcSpacePos.v[1] / aspectRatio, ndcSpacePos.v[2]);
	}
public:

	Horizon(GLuint shader_programme)
	{
		projectionMatrix = identity_mat4();
		viewMatrix = identity_mat4();

		float hPoints[] =
		{
			-10, 0, 0,
			10, 0, 0
		};


		//horizon
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &points_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
		glBufferData(GL_ARRAY_BUFFER, 3 * 2 * sizeof(GLfloat), &hPoints, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		//the part that was added
		glGenVertexArrays(1, &ad_vao);
		glBindVertexArray(ad_vao);

		glGenBuffers(1, &ad_points_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, ad_points_vbo);
		glBufferData(GL_ARRAY_BUFFER, 3 * 2 * sizeof(GLfloat), &hPoints, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		horizonFlag_location = glGetUniformLocation(shader_programme, "horizonFlag");
		horizonColor_location = glGetUniformLocation(shader_programme, "horizonColor");
	}

	bool BelowHorizon(Vector3 vertex)
	{
		//vertex = transform.InverseTransformPosition(vertex);
		vertex = TransformPoint(vertex);

		Vector3 left;
		Vector3 right;

		for (int i = 0; i < horizon.Size(); i++)
		{
			if (horizon.At(i).x < vertex.x)
			{
				left = horizon.At(i);
			}
			else if (horizon.At(i).x > vertex.x)
			{
				right = horizon.At(i);
				break;
			}
			else
			{
				if (left.x == vertex.x)
				{
					right = horizon.At(i);
					break;
				}
				else
				{
					left = horizon.At(i);
				}
			}
		}

		return !Above(left, right, vertex);
	}	

	void AddToHorizon(BoundingBox boundingBox)
	{
		addedHorizon.Clear();

		Vector3* upperPoints = boundingBox.GetUpperPoints();
		List<Vector3> upperPointsList;

		//Project upper points to 2D
		for (int i = 0; i < 4; i++)
		{
			Vector3 v = TransformPoint(upperPoints[i]);
			v.z = 0;
			upperPointsList.Add(v);
		}

		upperPointsList.InsertionSort(
			[](Vector3& a, Vector3& b) ->
			bool {
				if (a.x == b.x)
				{
					return a.y > b.y;
				}

				return a.x < b.x;
			});

		Vector3 a = upperPointsList[0];
		Vector3 b = upperPointsList[3];

		for (int up = 2; up > 0; up--)
		{
			if (!Above(a, b, upperPointsList[up]))
			{
				upperPointsList.RemoveAt(up);
			}
		}

		//Vector3 aL, aR, bL, bR;
		int aLIdx, aRIdx, bLIdx, bRIdx;

		//Get the adjecent points to define the borders
		GetAdjacentPoints(a, aLIdx, aRIdx);
		GetAdjacentPoints(b, bLIdx, bRIdx);

		//cut horizon, adding point where the lines intersect
		for (int up = upperPointsList.Size() - 2; up >= 0 && up < upperPointsList.Size(); up--)
		{
			for (int i = bRIdx-1; i >= aLIdx; i--)
			{
				Vector3 point;
				if (LinesIntersection(upperPointsList[up], upperPointsList[up + 1], horizon[i], horizon[i + 1], point))
				{
					horizon.AddAt(point, i + 1);
				}
			}
		}

		///Get the new adjecent points. We know its at least at aLIdx (the start of the borders)
		GetAdjacentPoints(a, aLIdx, aRIdx, aLIdx);
		GetAdjacentPoints(b, bLIdx, bRIdx, aLIdx);

		Vector3 aL = horizon[aLIdx], aR = horizon[aRIdx];
		Vector3 baseA, baseB;

		//create vertical lines that sustain the object

		LinesIntersection(horizon[aLIdx], horizon[aRIdx], a,
			a + Vector3(0, -100.0f, 0), baseA);

		LinesIntersection(horizon[bLIdx], horizon[bRIdx], b,
			b + Vector3(0, -100.0f, 0), baseB);

		int startI = aLIdx, endI = bRIdx, adI = 1;

		if (Above(horizon[bLIdx], horizon[bRIdx], b))
		{
			horizon.AddAt(baseB, bRIdx);
			horizon.AddAt(b, bRIdx);

			addedHorizon.AddAt(baseB, 0);
		}
		addedHorizon.AddAt(b, 0);
		//right upper point is on the index bRIdx now

		addedHorizon.AddAt(a, 0);
		if (Above(aL, aR, a))
		{
			horizon.AddAt(a, aLIdx + 1);
			horizon.AddAt(baseA, aLIdx + 1);

			addedHorizon.AddAt(baseA, 0);

			startI += 2;
			endI += 2;
			adI ++;
		}
		//left upper point is on the index aLIdx+2
		//right upper point is on the index bRIdx+2 now

		//add the remaining upper points (if above horizon)
		for (int up = upperPointsList.Size()-2; up > 0 && up < upperPointsList.Size(); up--)
		{
			int li, ri;

			GetAdjacentPoints(upperPointsList[up], li, ri, startI);

			if (Above(horizon[li], horizon[ri], upperPointsList[up]))
			{
				horizon.AddAt(upperPointsList[up], ri);
				addedHorizon.AddAt(upperPointsList[up], adI);

				endI++;
			}
		}

		//remove all point below the new horizon
		for (int up = upperPointsList.Size() - 2; up >= 0 && up < upperPointsList.Size(); up--)
		{
			for (int i = endI - 1; i > startI; i--)
			{
				Vector3 point;
				if (BelowSegment(upperPointsList[up], upperPointsList[up + 1], horizon[i], true))
				{
					horizon.RemoveAt(i);
					endI--;
				}
			}
		}

		UpdatePoints();
	}

	void ClearHorizon()
	{
		horizon.Clear();
		horizon.Add(Vector3(-10, -1 / aspectRatio, 0));
		horizon.Add(Vector3(10, -1 / aspectRatio, 0));

		addedHorizon.Clear();

		UpdatePoints();
	}

	void Render(GLuint model_mat_location)
	{
		glUniform1i(horizonFlag_location, 1);
		glUniform3f(horizonColor_location, 1, 1, 0);

		glUniformMatrix4fv(model_mat_location, 1, GL_FALSE, transform.GetModelMatrix().m);
		glBindVertexArray(vao);
		glDrawArrays(GL_LINE_STRIP, 0, horizon.Size());

		glUniform3f(horizonColor_location, 1, 0.2f, 0);

		glUniformMatrix4fv(model_mat_location, 1, GL_FALSE, transform.GetModelMatrix().m);
		glBindVertexArray(ad_vao);
		glDrawArrays(GL_LINE_STRIP, 0, addedHorizon.Size());

		glUniform1i(horizonFlag_location, 0);
	}

	void Update(Transform eye, mat4 projectionMatrix, float aspectRatio)
	{
		this->aspectRatio = aspectRatio;
		this->projectionMatrix = projectionMatrix;
		this->viewMatrix = eye.GetModelInverseMatrix();

		transform.Set(eye);
		transform.Move(eye.GetForward() * -(projectionMatrix.m[0]));
	}

	List<Vector3>* GetHorizon() 
	{
		return &horizon;
	}

	Transform* GetTransform()
	{
		return &transform;
	}
};

