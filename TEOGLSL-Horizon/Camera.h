#pragma once
#include "Transform.h"
#include "maths_funcs.h"

#define ONE_DEG_IN_RAD ( 2.0 * 3.14159265358979323846 ) / 360.0 // 0.017444444

class Camera
{
private:
	Transform transform;
	mat4 proj_mat;

	GLint view_mat_location;
	GLint proj_mat_location;

	void UpdateMatrix()
	{
		glUniformMatrix4fv(view_mat_location, 1, GL_FALSE, transform.GetModelInverseMatrix().m);
		glUniformMatrix4fv(proj_mat_location, 1, GL_FALSE, proj_mat.m);
	}
public:
	Camera(float aspectRatio, GLint view_mat_location, GLint proj_mat_location)
	{
		this->view_mat_location = view_mat_location;
		this->proj_mat_location = proj_mat_location;

		float near = 0.1f;	// clipping plane
		float far = 100.0f;	// clipping plane
		float fov = 67.0f * ONE_DEG_IN_RAD; // convert 67 degrees to radians

		// matrix components
		float inverse_range = 1.0f / tan(fov * 0.5f);
		float Sx = inverse_range / aspectRatio;
		float Sy = inverse_range;
		float Sz = -(far + near) / (far - near);
		float Pz = -(2.0f * far * near) / (far - near);
		//proj_mat = new GLfloat[16]{ Sx, 0.0f, 0.0f, 0.0f, 0.0f, Sy, 0.0f, 0.0f, 0.0f, 0.0f, Sz, -1.0f, 0.0f, 0.0f, Pz, 0.0f };

		proj_mat = mat4(Sx, 0.0f, 0.0f, 0.0f, 0.0f, Sy, 0.0f, 0.0f, 0.0f, 0.0f, Sz, -1.0f, 0.0f, 0.0f, Pz, 0.0f);

		//float l = -2, r = 2, t = 2, b = -2;
		//
		//proj_mat = new GLfloat[16]{ 2 / (r - l), 0.0f, 0.0f, 0.0f, 0.0f, 2 / (t - b), 0.0f, 0.0f, 0.0f, 0.0f, -2 / (far - near), 0, -(r + l) / (r - l), -(t + b) / (t - b), Sz, 1.0f };
	}
	~Camera()
	{

	}

	void Move(Vector3 move)
	{
		transform.Move(move * -1.0f);
		UpdateMatrix();
	}

	void RotateY(float y)
	{
		transform.Rotate(Vector3(0, y, 0));
		UpdateMatrix();
	}

	Transform* GetTransform()
	{
		return &transform;
	}

	mat4 GetProjectionMatrix()
	{
		return proj_mat;
	}
};

