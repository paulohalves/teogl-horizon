#version 410

#define ApproximatelyDelta 0.0001f

in vec3 position_eye, normal_eye;

uniform mat4 projection_mat, view_mat, cameraEye_view_mat;
uniform vec2 window_size;

uniform float horizon[64];
uniform int horizonSize;

uniform int horizonFlag;
uniform vec3 horizonColor;
uniform vec3 visible_color;

uniform int discardFragment;

//LIGHT
const vec3 lightPos = vec3(1.0, 2.0, 1.0);
const vec3 lightColor = vec3(1.0, 1.0, 1.0);

const float lightPower = 5.0;
const vec3 ambientColor = vec3(0.3, 0.3, 0.3);
const vec3 diffuseColor = vec3(0.5, 0.5, 0.5);
const vec3 specColor = vec3(1.0, 1.0, 1.0);
const float shininess = 100.0;

const float screenGamma = 2.2;

out vec4 fragment_colour; // final colour of surface

bool Above(vec3 a, vec3 b, vec3 c, bool allowColinear)
{
	if (c.x < a.x || c.x > b.x)
	{
		return false;
	}

	// ccw > 0: counter-clockwise; ccw < 0: clockwise; ccw = 0: collinear
	float ccw = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);

	if (abs(ccw) < ApproximatelyDelta)
	{
		if (allowColinear /*|| (c.y > a.y && c.y > b.y)*/)
		{
			return true;
		}
	}

	return ccw > 0;
}

bool BelowHorizon(vec3 vPos)
{
	vec3 left;
	vec3 right;

	for(int i = 0; i < horizonSize; i += 3)
	{
		vec3 hP = vec3(horizon[i], horizon[i+1], horizon[i+2]);
		if (hP.x < vPos.x)
		{
			left = vec3(horizon[i], horizon[i+1], horizon[i+2]);
		}
		else if (hP.x > vPos.x)
		{
			right = vec3(horizon[i], horizon[i+1], horizon[i+2]);
			break;
		}
		else
		{
			if (left.x == vPos.x)
			{
				right = hP;
				break;
			}
			else
			{
				left = hP;
			}
		}
	}

	return !Above(left, right, vPos, false);
}

vec3 CalculateLight(vec3 pos)
{
	vec3 Ia = length(ambientColor) * visible_color;

	vec3 normal = normalize(normal_eye);
	vec3 lightPosEye = vec3 (view_mat * vec4 (lightPos, 1.0));

	vec3 posEye = vec3(view_mat * (vec4(pos, 1.0)));
	vec3 objectToLight = lightPosEye - posEye;
	vec3 dirToLight = normalize(objectToLight);
	float distance = length(objectToLight);

	float dot_prod = dot (dirToLight, normal);
	dot_prod = max (dot_prod, 0.0);

	vec3 Id = visible_color * (lightPower/distance) * dot_prod; // final diffuse intensity

	vec3 surfaceToView = normalize (-posEye);
	vec3 half_eye = normalize (surfaceToView + dirToLight);
	float dot_prod_specular = max (dot (half_eye, normal), 0.0);
	float specularFactor = pow (dot_prod_specular, shininess);

	vec3 Is = specColor  * (lightPower/distance) * specularFactor; // final specular intensity

	return Is + Id + Ia;
}

void main () 
{
	if (horizonFlag == 1)
	{
		fragment_colour = vec4 (horizonColor, 1.0);
		return;
	}

	//projection_mat, view_mat, model_mat, cameraEye_view_mat;

	vec4 ndc = vec4(
        (gl_FragCoord.x / window_size.x - 0.5) * 2,
        (gl_FragCoord.y / window_size.y - 0.5) * 2,
        (gl_FragCoord.z * 2) - 1,
        //0,
        1);

	vec4 clip = ndc / gl_FragCoord.w;
	vec4 world = inverse(view_mat) * inverse(projection_mat) * clip;
	
	clip = projection_mat * (cameraEye_view_mat * world);
	ndc = clip / clip.w;

	//if (horizonFlag == 1)
	//{
	//	fragment_colour = vec4 (horizonColor, 1.0);
	//}
	if (BelowHorizon(vec3(ndc)))
	{
		if (discardFragment == 1)
		{
			discard;
			return;
		}
		fragment_colour = vec4 (vec3(1, 0, 0), 1.0);
	}
	else
	{
		vec3 color = CalculateLight(vec3(world));
		fragment_colour = vec4 (color, 1.0);
	}
}
